#ifndef DNest4_Template_MyConditionalPrior
#define DNest4_Template_MyConditionalPrior

#include "DNest4/code/DNest4.h"

class MyConditionalPrior:public DNest4::ConditionalPrior
{
	private:
        // Location and scale parameters for log_r conditional prior
        double location_log_r, scale_log_r;

		double perturb_hyperparameters(DNest4::RNG& rng);

	public:
		MyConditionalPrior();

		void from_prior(DNest4::RNG& rng);

		double log_pdf(const std::vector<double>& vec) const;
		void from_uniform(std::vector<double>& vec) const;
		void to_uniform(std::vector<double>& vec) const;

		void print(std::ostream& out) const;

        // Some static functions to do with Laplacian distributions
        static int sign(double x);
        static double laplacian_log_pdf(double x, double center, double width);
		static double laplacian_cdf(double x, double center, double width);
		static double laplacian_cdf_inverse(double x, double center, double width);
};

#endif

