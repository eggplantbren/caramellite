#include "MyConditionalPrior.h"
#include "DNest4/code/DNest4.h"
#include <cmath>

using namespace DNest4;

MyConditionalPrior::MyConditionalPrior()
{

}

void MyConditionalPrior::from_prior(RNG& rng)
{
    location_log_r = tan(M_PI*(0.01 + 0.98*rng.rand() - 0.5));
    scale_log_r = 3.0*rng.rand();
}

double MyConditionalPrior::perturb_hyperparameters(RNG& rng)
{
	double logH = 0.0;

    int which = rng.rand_int(2);
    if(which == 0)
    {
        location_log_r = (atan(location_log_r)/M_PI + 0.5 - 0.01)/0.98;
        location_log_r += rng.randh();
        wrap(location_log_r, 0.0, 1.0);
        location_log_r = tan(M_PI*(0.01 + 0.98*location_log_r - 0.5));
    }
    else
    {
        scale_log_r += 3.0*rng.randh();
        wrap(scale_log_r, 0.0, 3.0);
    }

	return logH;
}

// vec[0] is log(r)
// vec[1] is phi
double MyConditionalPrior::log_pdf(const std::vector<double>& vec) const
{
    double logp = 0.0;

    logp += laplacian_log_pdf(vec[0], location_log_r, scale_log_r);
    if(vec[1] < 0.0 || vec[1] > 2*M_PI)
        return -std::numeric_limits<double>::max();

	return logp;
}

void MyConditionalPrior::from_uniform(std::vector<double>& vec) const
{
    vec[0] = laplacian_cdf_inverse(vec[0], location_log_r, scale_log_r);
    vec[1] = 2.0*M_PI*vec[1];
}

void MyConditionalPrior::to_uniform(std::vector<double>& vec) const
{
    vec[0] = laplacian_cdf(vec[0], location_log_r, scale_log_r);
    vec[1] = vec[1]/(2.0*M_PI);
}

void MyConditionalPrior::print(std::ostream& out) const
{
	out<<location_log_r<<" "<<scale_log_r<<" ";
}

// Laplacian cdf stuff
int MyConditionalPrior::sign(double x)
{
	if(x == 0.)
		return 0;
	if(x > 0.)
		return 1;
	return -1;
}


double MyConditionalPrior::laplacian_log_pdf
                        (double x, double center, double width)
{
	assert(width > 0.0);
    return -log(2*width) - std::abs(x - center)/width;
}


double MyConditionalPrior::laplacian_cdf(double x, double center, double width)
{
	assert(width > 0.0);
	return 0.5 + 0.5*sign(x - center)*(1. - exp(-std::abs(x - center)/width));
}

double MyConditionalPrior::laplacian_cdf_inverse(double x, double center,
														double width)
{
	assert(width > 0.0);
	assert(x >= 0.0 && x <= 1.0);

	return center - width*sign(x - 0.5)*log(1. - 2.*std::abs(x - 0.5));
}


